<?php

/**
 * @file
 * Administration settings for drag_to_share button integration
 */

/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 */
function drag_to_share_admin_settings() {
  $form = array();

  $form['drag_to_share_general_notetypes'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Choose which node types to show Drag to Share in'),
  );
  $form['drag_to_share_general_notetypes']['drag_to_share_nodetypes'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Node Types'),
    '#description'   => t('Display an drag_to_share button for these node types.'),
    '#default_value' => variable_get('drag_to_share_nodetypes', array('page', 'story')),
    '#options'       => node_get_types('names'),
  );
  $form['drag_to_share_show_non_nodes'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Show Drag to Share in non node areas'),
	'#collapsible' => TRUE,
	'#collapsed' => TRUE,
  );
  $form['drag_to_share_show_non_nodes']['drag_to_share_non_nodes'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show Drag to Share in non-node areas'),
    '#default_value' => variable_get('drag_to_share_non_nodes', '0'),
    '#description'   => t('<p>This setting will ignore the Node Types setting above</p><p>Drag to Share will appear anywhere, such as on the front page and views - make sure you define CSS classes below to narrow down the images that get shared, this option will make a large number of images sharable. Be careful with this as putting Drag to Share on images that link might be confusing for the user.</p><p>Drag to Share links to the current page you are on, not the node that the image you are dragging belongs to. So having this show on your list of the latest 10 nodes might not be a good idea.</p>'),
  );
  $form['drag_to_share_general_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Choose which buttons to Show'),
  );
  $form['drag_to_share_general_settings']['drag_to_share_twitter'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Twitter'),
    '#default_value' => variable_get('drag_to_share_twitter', '1'),
    '#description'   => t('Display a Twitter button.'),
  );
  $form['drag_to_share_general_settings']['drag_to_share_delicious'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Delicious'),
    '#default_value' => variable_get('drag_to_share_delicious', '1'),
    '#description'   => t('Display a Delicious button.'),
  );
  $form['drag_to_share_general_settings']['drag_to_share_digg'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Digg'),
    '#default_value' => variable_get('drag_to_share_digg', '1'),
    '#description'   => t('Display a Digg button.'),
  );
  $form['drag_to_share_general_settings']['drag_to_share_facebook'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Facebook'),
    '#default_value' => variable_get('drag_to_share_facebook', '1'),
    '#description'   => t('Display a Facebook button.'),
  );

  $form['drag_to_share_classes'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter the classes you want to include, separating each definition with a comma'),
    '#default_value' => variable_get('drag_to_share_classes', 'img'),
    '#rows' => 5,
	'#cols' => 10,
    '#description' => t('Normally you will want to use this for images and "img" is the default. Its probably best to be much more specific than that, try something like "#main img", or even more specific with multiple image fields like ".image-field-1 img, .image-field-2 img"'),
    '#required' => FALSE,
  );
  return system_settings_form($form);
}