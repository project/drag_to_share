
-- SUMMARY --
The module allows you to share the node with Twitter, Delicious, Digg and Facebook
It does this without interrupting the structure of the page as nothing is shown until you hover over an image.
When hovering over an image you are prompted to drag it. On dragging the image the screen darkens and the logos for the social websites above animate onto the screen. A message with the image/page title and a small version of the image gets attached to the cursor so you can drop it on one of the logos. Dropping the message onto the logo opens a new window that shares the current node by passing the url and title.

Here is the module in action (rollover the image to test):
http://01am.co.uk/blog/testing-all-browsers-mac-and-pc

The inspiration for this module was the Meebo Toolbar which can be found here:
http://business.meebo.com/demo/
The code got a lot of help from this tutorial by Dan Wellman:
http://net.tutsplus.com/tutorials/javascript-ajax/drag-to-share/

For a full description of the module, visit the project page:
  http://drupal.org/project/drag_to_share

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/drag_to_share


-- REQUIREMENTS --

* This module requires that Jquery UI is installed, get it here: http://drupal.org/project/jquery_ui


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure the module in Administer >> Site configuration >> Drag to share

  - select what node types the module will add the 'drag to share' functionality
  - add custom CSS selectors to narrow down which images get used

  As default the module has 'img' in the classes box, but you may not want all images getting the Drag to Share treatment. Here you can narrow down which images you want to choose. For instance if a node has a main header image it might be selected by doing something like this:

.node-header img

  You can add multiple classes in here by separating them with commas.


-- TROUBLESHOOTING --

* If the 'Drag to share' text and a custom cursor does not display when you hover over an image check the following
	
  - is the module enabled
  - are you on a node page
  - have you enabled that node type
  - are you using a css selector that doesn't exist, or is defined badly. Try using 'img' first to get any image.


-- CONTACT --

Current maintainers:
* Alistair McClymont - http://drupal.org/user/121106
