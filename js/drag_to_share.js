
/*
This javascipt heavily borrows from the excellent tutorial by Dan Wellman here:
http://net.tutsplus.com/tutorials/javascript-ajax/drag-to-share/
*/
$(function() {
	if (($.browser.msie && $.browser.version.substr(0,1)>6) || !$.browser.msie){
		// add the targets
		var drag_to_share_dropnumber = 0;
		var drag_to_share_html = '<div id="drag-to-share-targets-wrapper"><ul id="drag-to-share-targets">';
		if(Drupal.settings.drag_to_share.twitter){
			drag_to_share_html += '<li id="twitter"><a href="http://twitter.com"><!-- --></a></li>';
			drag_to_share_dropnumber++;
		}
		if(Drupal.settings.drag_to_share.delicious){
			drag_to_share_html += '<li id="delicious"><a href="http://delicious.com"><!-- --></a></li>';
			drag_to_share_dropnumber++;
		}
		if(Drupal.settings.drag_to_share.digg){
			drag_to_share_html += '<li id="digg"><a href="http://digg.com"><!-- --></a></li>';
			drag_to_share_dropnumber++;
		}
		if(Drupal.settings.drag_to_share.facebook){
			drag_to_share_html += '<li id="facebook"><a href="http://www.facebook.com"><!-- --></a></li>';
			drag_to_share_dropnumber++;
		}
		drag_to_share_html += '</ul></div>';
		$('body').append(drag_to_share_html);
	
		// position the targets and set the timing
		var drag_to_share_top = -80;
		var drag_to_share_bottom = 103;
		var drag_to_share_time = 500;
		var drag_to_share_difference = drag_to_share_top - drag_to_share_bottom;
		//$("#drag-to-share-targets").css("left", (($(window).width() / 2) - $("#drag-to-share-targets").width() / 2)+10);
		$("#drag-to-share-targets li:last-child").addClass("last");
		
		$("#drag-to-share-targets-wrapper").css({width: $("#drag-to-share-targets").width()});
		$("#drag-to-share-targets").css({top:drag_to_share_bottom});
	
		// cache the selector
		var drag_to_share_images = $(Drupal.settings.drag_to_share.classes);
		// either get the title from the image title or the page title
		var title = $("title").text() || document.title;

		// make images draggable
		// this function starts everything off when you begin to drag
		drag_to_share_images.draggable({
			// create draggable helper
			helper: function() {
				return $("<div>").attr("id", "drag-to-share-helper").html("<span>" + title + "</span><img id='drag-to-share-thumb' src='" + $(this).attr("src") + "'>").appendTo("body");
			},
			cursor: "pointer",
			cursorAt: { left: -20, top: 20 },
			zIndex: 99999,
			// show overlay and targets
			start: function() {
				// add the overlay div to the body
				if (!$.browser.msie){
					$("<div>").attr("id", "drag-to-share-overlay").css("opacity", 0).appendTo("body");
					$("#drag-to-share-overlay").fadeTo("slow", 0.7);	
				} else {
					$("<div>").attr("id", "drag-to-share-overlay").css("opacity", 0.7).appendTo("body");
				}
				$("#drag-to-share-tip").remove();
				$(this).unbind("mouseover");
				//$("#drag-to-share-targets").css("left", ($("body").width() / 2) - $("#drag-to-share-targets").width() / 2);
			
				// make the targets visible and animate them
				$("#drag-to-share-targets li a").css({top: drag_to_share_difference});
				$("#drag-to-share-targets").css( {display: "block"} );
				$("#drag-to-share-targets li a").stop().animate(
					{top:0}, 
					{duration:drag_to_share_time});
			},
			// remove targets and overlay
			stop: function() {
				// animate the targets then make them invisible
				$("#drag-to-share-targets li a").stop().animate(
				{top:drag_to_share_difference}, (drag_to_share_time/2), function() {
					$("#drag-to-share-targets").css( {display: "none"} );
				});

				$(".drag-to-share-share").remove();
				$(this).bind("mouseover", drag_to_share_createTip);
				// this looks good - but is fairly slow
				$("#drag-to-share-overlay").fadeTo("slow", 0, function(){
					$("#drag-to-share-overlay").remove();$("#drag-to-share-overlay").remove();
				});
			
			}
		});

		// make targets droppable
		// this function does stuff when you rollover and drop on the targets
		$("#drag-to-share-targets li").droppable({
			tolerance: "pointer",
			//show info when over target
			over: function() {
				$(".drag-to-share-share").remove();
				$("<span>").addClass("drag-to-share-share").text("Share on " + $(this).attr("id")).addClass("active").appendTo($(this)).fadeIn('slow');
				var id = $(this).attr("id");
				if (id.indexOf("twitter") != -1) {
					$("#twitter a").css({backgroundPosition: '0 -240px'})
				} else if (id.indexOf("delicious") != -1) {
					$("#delicious a").css({backgroundPosition: '0 -300px'})
				} else if (id.indexOf("digg") != -1) {
					$("#digg a").css({backgroundPosition: '0 -420px'})
				} else if (id.indexOf("facebook") != -1) {
					$("#facebook a").css({backgroundPosition: '0 -360px'})
				}
			},
			out: function() {
				var id = $(this).attr("id");
				if (id.indexOf("twitter") != -1) {
					$("#twitter a").css({backgroundPosition: '0 0'})
				} else if (id.indexOf("delicious") != -1) {
					$("#delicious a").css({backgroundPosition: '0 -60px'})
				} else if (id.indexOf("digg") != -1) {
					$("#digg a").css({backgroundPosition: '0 -180px'})
				} else if (id.indexOf("facebook") != -1) {
					$("#facebook a").css({backgroundPosition: '0 -120px'})
				}
			},
			drop: function() {
				var id = $(this).attr("id"),
				currentUrl = window.location.href,
				baseUrl = $(this).find("a").attr("href");

				if (id.indexOf("twitter") != -1) {
					window.open(baseUrl + "/home?status=" + title + ": " + currentUrl, "Twitter");
					// add this to the end of the above line to cusomise it
					// , "height=650,width=1075,left=100,top=100,status=1,scrollbars=1,location=1,toolbar=1;resizable=1"
				} else if (id.indexOf("delicious") != -1) {
					window.open(baseUrl + "/save?url=" + currentUrl + "&title=" + title, "Delicious");
				} else if (id.indexOf("digg") != -1) {
						window.open(baseUrl + "/submit?url=" + currentUrl + "&amp;title" + title, "Digg");
				} else if (id.indexOf("facebook") != -1) {
				window.open(baseUrl + "/sharer.php?u=" + currentUrl + "&t=" + title, "Facebook");
				}
			}		  
		});

		var drag_to_share_createTip = function(e) {
			//create tool tip if it doesn't exist
			($("#drag-to-share-tip").length === 0) ? $("<div>").html("<span>Drag this image to share the page<\/span><span class='arrow'><\/span>").attr("id", "drag-to-share-tip").css({ left:e.pageX + 30, top:e.pageY - 16 }).appendTo("body").fadeIn(1000) : null;
		};

		drag_to_share_images.bind("mouseover", drag_to_share_createTip);

		drag_to_share_images.mousemove(function(e) {

			//move tooltip
			$("#drag-to-share-tip").css({ left:e.pageX + 30, top:e.pageY - 16 });
		});

		drag_to_share_images.mouseout(function() {

			//remove tooltip
			$("#drag-to-share-tip").remove();
		});
	}
});